@extends('layouts.app')

@section('content')
<div class="container">
    <div class="mt-3 ml-3 mr-3">
        <div class="card">

            <div class="card-header">
              <h3 class="card-title">Posts Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif

              <table class="table table-bordered">
                <thead><tr>
                  <th style="width: 10px">#</th>
                  <th>Title</th>
                  <th>Body</th>
                  <th style="width: 40px">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($posts as $key => $post)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $post->title }}</td>
                    <td>{{ $post->body }}</td>
                    <td style="display: flex;">
                        <a href="/posts/{{$post->id}}" class="btn btn-default btn-sm">read</a>

                    </td>
                    </tr>

                    @empty
                        <tr>
                            <td colspan="4" align="center">No Posts</td>
                        </tr>

                @endforelse
              </tbody></table>
            </div>
          </div>
    </div>
</div>
@endsection
