@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3 mr-3">
        <div class="card">

            <div class="card-header">
              <h3 class="card-title">User List</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                <!--<a class="btn btn-primary mb-2" href="/profiles/create">Create New Post</a>-->
              <table class="table table-bordered">
                <thead><tr>
                  <th style="width: 10px">#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th style="width: 40px">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($profiles as $key => $profile)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $profile->name }}</td>
                    <td>{{ $profile->email }}</td>
                    <td style="display: flex;">
                        <a href="/profiles/{{$profile->id}}" class="btn btn-info btn-sm">show</a>
                        <a href="/profiles/{{$profile->id}}/edit" class="btn btn-default btn-sm">edit</a>
                        <form action="/profiles/{{$profile->id}}" method="POST">
                          @csrf
                          @method('DELETE')
                          <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                    </tr>

                    @empty
                        <tr>
                            <td colspan="4" align="center">No profiles</td>
                        </tr>

                @endforelse
              </tbody></table>
            </div>
          </div>
    </div>
@endsection
