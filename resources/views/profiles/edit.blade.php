@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Edit Profile {{$profile->id}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="/profiles/{{$profile->id}}" method="POST">
        @csrf
        @method('PUT')
      <div class="box-body">
        <div class="form-group">
          <label for="name">username</label>
          <input type="text" class="form-control" name="name" id="name" value="{{old('name', $profile->name)}}" placeholder="Enter username">
          @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
          <label for="email">email</label>
          <input type="text" class="form-control" name="email" id="email" value="{{old('email', $profile->email)}}"placeholder="email">
          @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
      </div>
      <!-- /.box-email -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
@endsection
