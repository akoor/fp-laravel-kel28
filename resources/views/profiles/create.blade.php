@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Profile username <b>{{Auth::user()->name}}</b> masih kosong</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="/profiles" method="POST">
        @csrf
      <div class="box-body">
        <div class="form-group">
          <label for="full_name">full names</label>
          <input type="text" class="form-control" name="full_name" id="full_name" value="{{old('full_name', '')}}" placeholder="Enter full_name">
          @error('full_name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
          <label for="phone">phone number</label>
          <!-- <textarea name="phone" id="phone" form="usrform" class="form-control">{{old('phone', '')}}</textarea> -->
          <input type="text" class="form-control" name="phone" id="phone" value="{{old('phone', '')}}"placeholder="phone">
          @error('phone')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="photo">photo</label>
            <input type="file" class="form-control" name="photo" id="photo" value="{{old('photo'),''}}" placeholder="Pisahkan dengan koma, contoh : postingan, beritaterkini, update">
        </div>
      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
@endsection
