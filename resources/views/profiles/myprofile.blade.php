@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Edit Profile {{old('id', Auth::user()->profile->full_name)}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="/profiles/{{old('id', Auth::user()->profile->full_name)}}" method="POST">
        @csrf
        @method('PUT')
      <div class="box-body">
        <div class="form-group">
          <label for="full_name">full name</label>
          <input type="text" class="form-control" name="full_name" id="full_name" value="{{old('full_name', Auth::user()->profile->full_name)}}" placeholder="Enter username">
          @error('full_name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
          <label for="phone">phone</label>
          <input type="text" class="form-control" name="phone" id="phone" value="{{old('phone', Auth::user()->profile->phone)}}"placeholder="phone">
          @error('phone')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="photo">Photo</label>
            <input type="text" class="form-control" name="photo" id="photo" value="{{old('photo', Auth::user()->profile->photo)}}"placeholder="photo">
            @error('photo')
                  <div class="alert alert-danger">{{ $message }}</div>
              @enderror
          </div>
      </div>
      <!-- /.box-email -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
@endsection
