@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Create New Post</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="/posts" method="POST">
        @csrf
      <div class="box-body">
        <div class="form-group">
          <label for="tag">Tags</label>
          <input type="text" class="form-control" name="tag" id="tag" value="{{old('tag', '')}}" placeholder="Enter tag">
          @error('tag')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
@endsection
