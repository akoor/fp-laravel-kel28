@extends('adminlte.master')

@section('content')
    <div class="mt-3 ml-3 mr-3">
        <div class="card">

            <div class="card-header">
              <h3 class="card-title">Tags Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                <a class="btn btn-primary mb-2" href="/tags/create">Create New Tag</a>
              <table class="table table-bordered">
                <thead><tr>
                  <th style="width: 10px">#</th>
                  <th>Tag</th>
                  <th style="width: 40px">Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($tags as $key => $tag)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $tag->tag_name }}</td>
                    <td style="display: flex;">
                        <a href="/tags/{{$tag->id}}" class="btn btn-info btn-sm">show</a>
                        <a href="/tags/{{$tag->id}}/edit" class="btn btn-default btn-sm">edit</a>
                        <form action="/tags/{{$tag->id}}" method="POST">
                          @csrf
                          @method('DELETE')
                          <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                    </tr>

                    @empty
                        <tr>
                            <td colspan="4" align="center">No Posts</td>
                        </tr>

                @endforelse
              </tbody></table>
            </div>
            <!-- /.card-body -->
            {{-- <div class="card-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                <li><a href="#">«</a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">»</a></li>
              </ul>
            </div> --}}
          </div>
    </div>
@endsection
