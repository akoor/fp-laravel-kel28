@extends('adminlte.master')

@section('content')
<div class="mt-3 ml-3 mr-3">
    <h4>Tag : {{$tag->tag_name}}</h4>


        <table class="table table-bordered">
            <thead><tr>
              <th style="width: 10px">#</th>
              <th>Title</th>
              <th>Body</th>
              <th>Author</th>
            </tr>
            </thead>
            <tbody>
            @forelse ($tag->posts as $key => $post)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $post->title }}</td>
                <td>{{ $post->body }}</td>
                <td style="display: flex;">
                    Penulis
                </td>
                </tr>

                @empty
                    <tr>
                        <td colspan="4" align="center">No Posts at tag : {{$tag->tag_name}}</td>
                    </tr>

            @endforelse
            <tbody>
            </table>
</div>
@endsection
