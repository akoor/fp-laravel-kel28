@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3">
<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">Edit Tag {{$tag->id}}</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form role="form" action="/tags/{{$tag->id}}" method="POST">
        @csrf
        @method('PUT')
      <div class="box-body">
        <div class="form-group">
          <label for="tag_name">Tags</label>
          <input type="text" class="form-control" name="tag_name" id="tag_name" value="{{old('tag', $tag->tag_name)}}" placeholder="Enter tag">
          @error('tag_name')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
@endsection
