<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Route::get('/master', function(){
//     return view('adminlte.master');
// });x

//post
Route::get('/posts', 'PostController@index');
Route::get('/posts/create', 'PostController@create');
Route::post('/posts', 'PostController@store');
Route::get('/posts/{post_id}', 'PostController@show');
Route::get('/posts/{post_id}/edit', 'PostController@edit');
Route::put('/posts/{post_id}', 'PostController@update');
Route::delete('/posts/{post_id}', 'PostController@destroy');
Route::get('/posts_all', 'PostController@indexall');


//tag
Route::get('/tags', 'tagController@index');
Route::get('/tags/create', 'tagController@create');
Route::post('/tags', 'tagController@store');
Route::get('/tags/{tag_id}', 'tagController@show');
Route::get('/tags/{tag_id}/edit', 'tagController@edit');
Route::put('/tags/{tag_id}', 'tagController@update');
Route::delete('/tags/{tag_id}', 'tagController@destroy');

//profile
Route::get('/profiles', 'ProfileController@index');
Route::get('/profiles/create', 'ProfileController@create');
Route::post('/profiles', 'ProfileController@store');
Route::get('/profiles/{profile_id}', 'ProfileController@show');
Route::get('/profiles/{profile_id}/edit', 'ProfileController@edit');
Route::put('/profiles/{profile_id}', 'ProfileController@update');
Route::delete('/profiles/{profile_id}', 'ProfileController@destroy');
Route::get('/profiles/{profile_id}/editprofile', 'ProfileController@editprofile');

Route::get('/myprofiles/', 'ProfileController@indexfull');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomeController@home');

//user
Route::get('/users', 'UserController@index');
Route::get('/users/create', 'UserController@create');
Route::post('/users', 'UserController@store');
Route::get('/users/{user_id}', 'UserController@show');
Route::get('/users/{user_id}/edit', 'UserController@edit');
Route::put('/users/{user_id}', 'UserController@update');
Route::delete('/users/{user_id}', 'UserController@destroy');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});
