<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

use DB;
use Auth;
use App\Post;
use App\Tag;

class PostController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function create(){
        return view('posts.create');
    }

    public function store(Request $request){

        $validatedData = $request->validate([
            'title' => 'required|unique:posts|max:255',
            'body' => 'required'
        ]);


        // dd($request->tags);
        $tags_arr = explode(',', $request['tags']);
        foreach($tags_arr as $index => $tag)
            $tags_arr[$index] = trim($tag);
        // dd($tag_arr);

        $tag_ids = [];
        foreach($tags_arr as $tag_name){

            $tag = Tag::where(["tag_name" => $tag_name])->first();
            if($tag){
                $tag_ids[] = $tag->id;
            }else{
                $new_tag = Tag::create(["tag_name" => $tag_name]);
                $tag_ids[] = $new_tag->id;
            }
        }

        // dd($tags_arr);

        $post = Post::create([
            "title" => $request["title"],
            "body" => $request["body"],
            "user_id" => Auth::id()
            // "user_id" => Auth::user()->id
        ]);

        $post->tags()->sync($tag_ids);

        // $user = Auth::user();
        // $user->posts()->save($post);
        Alert::success('BERHASIL', 'Berhasil Menambahkan Data');

        return redirect('/posts/'); //->with('success', 'Post Berhasil Disimpan')
    }

    public function update($id, Request $request){
        $validatedData = $request->validate([
            'title' => 'required|unique:posts|max:255',
            'body' => 'required'
        ]);

        $post = Post::where('id', $id)->update([
            "title" => $request["title"],
            "body" => $request["body"]
        ]);
        Alert::success('BERHASIL', 'Berhasil Mengedit Data');
        return redirect('/posts/');
    }

    public function destroy($id){
        $query = DB::table('posts')->where('id', $id)->delete();
        Alert::success('BERHASIL', 'DATA BERHASIL DI HAPUS');
        return redirect('/posts/');
    }

    public function indexall(){
        $posts = Post::all();

        // $user = Auth::user();
        // $posts = $user->posts;

        return view('posts.indexfull', compact('posts'));
    }

    public function index(){
        // $posts = Post::all();

        $user = Auth::user();
        $posts = $user->posts;
        // dd($user);

        return view('posts.index', compact('posts'));
    }

    public function show($id){
        $post = Post::find($id);

        // dd($post->author);

        return view('posts.show', compact('post'));
    }

    public function edit($id){
        $post = Post::find($id);
        return view('posts.edit', compact('post'));
    }
}
