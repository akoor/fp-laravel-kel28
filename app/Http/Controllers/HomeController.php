<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Post;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['home']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Post::all();

        // $user = Auth::user();
        // $posts = $user->posts;

        return view('posts.index', compact('posts'));
    }

    public function home(){
        $posts = Post::all();

        // $user = Auth::user();
        // $posts = $user->posts;

        return view('Welcome', compact('posts'));
    }
}
