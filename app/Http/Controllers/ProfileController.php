<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use Auth;
use App\User;
use App\Profile;

class ProfileController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }


    public function create(){
        return view('profiles.create');
    }

    public function store(Request $request){

        $validatedData = $request->validate([
            'full_name' => 'required|unique:profiles|max:255',
            'phone' => 'required'
        ]);

        // dd($tags_arr);

        $profile = Profile::create([
            "full_name" => $request["full_name"],
            "phone" => $request["phone"],
            "photo" => $request["photo"],
            "user_id" => Auth::id()
            // "user_id" => Auth::user()->id
        ]);

        return redirect('/profiles/')->with('success', 'Post Berhasil Disimpan');
    }

    public function store_profile(Request $request){

        $validatedData = $request->validate([
            'full_name' => 'required|unique:profiles|max:255',
            'email' => 'required'
        ]);

        // dd($tags_arr);

        $profile = User::create([
            "name" => $request["name"],
            "email" => $request["email"],
            "user_id" => Auth::id()
            // "user_id" => Auth::user()->id
        ]);

        return redirect('/profiles/')->with('success', 'Post Berhasil Disimpan');
    }



    public function update($id, Request $request){
        $validatedData = $request->validate([
            'name' => 'required|unique:users|max:255',
            'email' => 'required'
        ]);

        $post = User::where('id', $id)->update([
            "name" => $request["name"],
            "email" => $request["email"]
        ]);

        return redirect('/profiles/')->with('success', 'Berhasil update profile');
    }

    public function destroy($id){
        $query = DB::table('users')->where('id', $id)->delete();
        return redirect('/profiles/')->with('success','Post berhasil di hapus!');
    }

    public function index(){
        $profiles = User::all();

        // $user = Auth::user();
        // $profiles = $user->profiles;

        return view('profiles.index', compact('profiles'));
    }

    public function indexfull(){
        // $profiles = User::all();

        $user = Auth::user();

        $profiles = $user->profile;
        if($profiles == null)
        return view('profiles.create', $user);


        // dd($user);

        return view('profiles.myprofile', compact('profiles'));
    }

    public function show($id){
        $profile = User::find($id);

        // dd($profile->author);

        return view('profiles.show', compact('profile'));
    }

    public function edit($id){
        $profile = User::find($id);
        return view('profiles.edit', compact('profile'));
    }

    public function editprofile($id){
        $profile = User::find($id);
        return view('profiles.editprofile', compact('profile'));
    }
}
