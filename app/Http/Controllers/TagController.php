<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use DB;
use App\Tag;

class TagController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function create(){
        return view('tags.create');
    }

    public function store(Request $request){

        $validatedData = $request->validate([
            'tag' => 'required|unique:tags|max:255'
        ]);


        // dd($request->tags);
        $tags_arr = explode(',', $request['tags']);
        foreach($tags_arr as $index => $tag)
            $tags_arr[$index] = trim($tag);
        // dd($tag_arr);

        $tag_ids = [];
        foreach($tags_arr as $tag_name){

            $tag = Tag::where(["tag_name" => $tag_name])->first();
            if($tag){
                $tag_ids[] = $tag->id;
            }else{
                $new_tag = Tag::create(["tag_name" => $tag_name]);
                $tag_ids[] = $new_tag->id;
            }

        }

        // dd($tags_arr);

        $tag = Tag::create([
            "title" => $request["title"]
        ]);
        Alert::success('BERHASIL', 'DATA BERHASIL DI TAMBAH');
        return redirect('/tags/');
    }

    public function update($id, Request $request){
        // dd ($request);


        $validatedData = $request->validate([
            'tag_name' => 'required|unique:tags|max:255',
        ]);

        $tag = Tag::where('id', $id)->update([
            'tag_name' => $request['tag_name']
        ]);
        Alert::success('BERHASIL', 'DATA BERHASIL DI UPDATE');
        return redirect('/tags/');
    }

    public function destroy($id){
        $query = DB::table('tags')->where('id', $id)->delete();
        Alert::success('BERHASIL', 'DATA BERHASIL DI HAPUS');
        return redirect('/tags/');
    }

    public function index(){
        $tags = Tag::all();

        // $user = Auth::user();
        // $tags = $user->tags;

        return view('tags.index', compact('tags'));
    }

    public function show($id){
        $tag = Tag::find($id);

        // dd($post->author);

        return view('tags.show', compact('tag'));
    }

    public function edit($id){
        $tag  = Tag::find($id);
        return view('tags.edit', compact('tag'));
    }
}
